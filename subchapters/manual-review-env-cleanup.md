## Review environment cleanup (if you would like to do it after finishing this lab)

To manually cleanup the review environment, you have two options:

1. The easiest option is to navigate to the pipeline associated with the specific merge request (accessible from the Merge Request detail window) and then click on the *Play All* button next to the **Cleanup** stage:

<img src="../images/fflag/51-review-pipeline.png" width="75%" height="75%">

**OR**

2. Do the following two things:

  - Manually stop the review environment associated with the specific merge request from the **Operations > Environments** window:

<img src="../images/fflag/52-stop-review-env.png" width="75%" height="75%">

  - Manually delete the branch associated with the specific merge request from the **Repository > Branches** window:

<img src="../images/fflag/53-del-mr-branch.png" width="75%" height="75%">
